Süßkartoffel-Kumpir mit Linsen-Rucola-Salat
======================

.. image:: images/suesskartoffel-kumpir.jpg

Zutaten
--------
für 2 Portionen

* 2 Süßkartoffeln
* 1 rote Zwiebel
* 1 Knoblauchzehe
* 1 Dose braune Linsen
* 4 g Gemüsebrühe
* 2 g Gewürzmischung "Mezze"
* 2 g Gewürzmischung "Harissa"
* 1 Zitrone
* 50 g Rucola
* 125 g Kirschtomaten
* 12 ml Balsamico Creme
* 10 gr Petersilie glatt u. Schnittlauch gemischt
* 100 gr Joghurth
* 20 gr. Butter
* 100 gr. Hirtenkäse
* Öl
* Wasser
* Salz
* Pfeffer



Zubereitung:
------------

1. Backofen auf 220 C Ober-/Unterhitze (200 C Umluft) vorheizen.
2. Gewaschene, ungeschälte Süßkartoffeln längs halbieren, Schnittfläche mit einer Gabel einstechen, mit etwas Öl besprühen oder bestreichen, mit Salz und Pfeffer würzen und mit der Schnittfläche nach unten auf ein mit Backpapier belegtes Backblech legen.2 Süßkartoffeln
3. Auf der mittleren Schiene im Backofen 30 - 35 Min. backen, bis sie weich sind.
4. Kirschtomaten für die letzten 10 Min. daneben legen.
5. Linsen in ein Sieb gießen und unter fließendem Wasser abspülen bis das Wasser klar durch läuft.
6. Rote Zwiebel abziehen, halbieren und in dünne Streifen schneiden.
7. Knoblauch abziehen.
8. In einer großen Pfanne etwas Öl erhitzen.
9. Zwiebelstreifen darin bei mittlerer Hitze 2 - 3 Min. anbraten.
10. Linsen, 50 ml Wasser, Gemüsebrühe, Mezze und Harissa dazugeben. Knoblauch hineinpressen und alles 2 - 3 Min. köcheln lassen, bis die Flüssigkeit fast verkocht ist. Mit Salz und Pfeffer würzen und etwas abkühlen lassen.
11. Schnittlauch in feine Röllchen schneiden.
12. Blätter der Petersilie abzupfen und fein hacken.
13. Zitrone heiß abwaschen. Schale der Zitrone abreiben, Zitrone in 6 Spalten schneiden.
14. In einer kleinen Schüssel Joghurth mit 1 TL Zitronenabrieb, der Hälfte der Kräuter und etwas Zitronensaft verrühren. Mit Salz und Pfeffer abschmecken.
15. In einer großen Schüssel Saft von 3 Zitronenspalten und 1 EL Joghurtdip verrühren.
16. Linsen unterrühren und Rucola unterheben. Mit Salz und Pfeffer und eventuell etwas mehr Zitronensaft abschmecken.
17. Nach dem Backen die Süßkartoffelhälften umdrehen und auf Teller verteilen. Butter auf die Süßkartoffeln geben und das Innere mit einer Gabel zerdrücken. Hirtenkäse darüber bröseln undmit restlichen Kräutern toppen.
18. Schmortomaten und Linsen Rucola-Salat daneben anrichten. Tomaten mit Balsamico-Creme beträufeln und alles mit dem Joghurth-Dip genießen.


Die Nährwerte (pro Portion)
-----------------------------------------------
* 793 kcal
* 28,49 g Fett
* 97,64 g Kohlenhydrate
* 29,22 g Eiweiß
