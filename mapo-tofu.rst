Mapo Tofu mit Broccoli
======================

Zutaten
--------

für 2 Portionen

* 1 tsp Szechuanpfeffer
* 1 Block Soft Tofu
* 1 tbsp Doubanjiang (Chili Bean Paste)
* 1 Knoblauchzehe
* Ingwer
* Frühlingszwiebeln
* 250g Hackfleisch (Rind)
* 1 tbsp Dou Chi (Fermented Black Beans)
* Chillipulver
* 1-2 tbsp Sojasoße
* 1 tbsp Dunkler Essig
* 1 tbsp Stärke
* 1 tbsp Öl
* 300g Broccoli

Zubereitung:
------------

1.  Szechuanpfeffer toasten und mahlen
2.  Tofu in 2cm große Würfel schneiden und in kochendes Salzwasser legen (2-3min)
3.  Broccoli auftauen / kochen
4.  Ingwer, Knoblauch und Frühlingszwiebeln schneiden
5.  Hackfleisch im Wok anbraten
6.  Doubanjiang und Dou Chi hinzufügen
7.  Knoblauch, Ingwer und Gewürze hinzufügen
8.  Tofu hinzufügen
9.  Stärke in Wasser (2 tbsp) lösen und damit Soße eindicken
10. Broccoli hinzufügen
11. Auf Teller servieren und mit Frühlingszwiebeln garnieren

Die Nährwerte ()
-----------------------------------------------

